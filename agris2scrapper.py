'''
        Paco Gallardo & Jaakko Lappalainen 2013.

        Gets AGRIS records and writes input files for 
                gs-scraper: https://bitbucket.org/ieru/gs-scraper

'''

from SPARQLWrapper import SPARQLWrapper, JSON
import MySQLdb
import uuid

sparql = SPARQLWrapper("http://202.45.142.113:10035/repositories/agris")

maxRecords = 4500000
i = 0
offset = 0
limit = 10000
sw = True
while sw:
        queryString = "select distinct ?p where {?o dcterms:title ?p} offset " + str(offset) + " limit " + str(limit)
        #print queryString
        sparql.setQuery(queryString)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        offset = offset + limit
        if i == maxRecords:
                sw = False
        if not results:
                sw =  False
        file = open(str(uuid.uuid4()),'w')
        for result in results["results"]["bindings"]:
                res = result["p"]["value"]
                res = res.encode('utf-8')
                res = res.replace(',','')
                file.write(''+res+', ')
        file.flush()
        file.close()